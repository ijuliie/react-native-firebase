import React, { useState, useContext } from "react"
import { createAppContainer } from "react-navigation";
import { NavigationContainer } from "@react-navigation/native"
import { createStackNavigator } from "@react-navigation/stack";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs"
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { MaterialIcons, AntDesign } from '@expo/vector-icons';
import HomeScreen from "./src/screens/HomeScreen";
import SignUpScreen from "./src/screens/SignUpScreen";
import SignInScreen from "./src/screens/SignInScreen";
import ResetScreen from "./src/screens/ResetScreen";
import WordCloudScreen from "./src/screens/WordCloudScreen"
import DashboardScreen from "./src/screens/DashboardScreen";
import WordListScreen from "./src/screens/WordListScreen"
import { Text } from "react-native"
import { firebase } from "./src/config"

const Stack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();

export default function App() {
    const [loggedIn, setLoggedIn] = useState(false)

    const HomeScreenStack = () => (
        <Stack.Navigator>
          <Stack.Screen name="SignIn" options={{ title: "Sign In" }}>
            {props => <SignInScreen 
            {...props} loggedIn={loggedIn} setLoggedIn={setLoggedIn} 
            />}
          </Stack.Screen>
          <Stack.Screen name="SignUp" options={{ title: "Sign Up" }}>
            {props => <SignUpScreen 
              {...props} loggedIn={loggedIn} setLoggedIn={setLoggedIn} 
            />}
          </Stack.Screen>
          <Stack.Screen name="Reset" options={{ title: "Forgot Password" }} component={ResetScreen} />
        </Stack.Navigator>
    )

    return (
        <NavigationContainer>
          <Tab.Navigator 
            initialRouteName="Home"
            activeColor="#000"
            inactiveColor="#fff"
            shifting={false}
            barStyle={{ backgroundColor: '#694fad' }}
          >
              <Tab.Screen 
                name="Home" 
                component={HomeScreenStack}
                options={{
                  tabBarLabel: "Home",
                  tabBarIcon: () =>
                    <MaterialCommunityIcons  name="home" color="fff" size={26} />
                }}
              />
              <Tab.Screen
                name="Dashboard"
                options={{
                  tabBarLabel: "Dashboard",
                  tabBarIcon: () =>
                    <MaterialIcons  name="dashboard" color="fff" size={26} />
                }}
              >
                {props => <DashboardScreen 
                  {...props} loggedIn={loggedIn} setLoggedIn={setLoggedIn} 
                />}
              </Tab.Screen>
              <Tab.Screen 
                name="Word Cloud" 
                component={WordCloudScreen}
                options={{
                  tabBarLabel: "Word Cloud",
                  tabBarIcon: () =>
                    <AntDesign name="cloudo" color="fff" size={26} />
                }} 
              />
              <Tab.Screen name="Word List" component={WordListScreen} />
          </Tab.Navigator>
        </NavigationContainer>
    )
}