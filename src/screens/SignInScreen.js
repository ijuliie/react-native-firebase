import React, { useState, useEffect } from "react"
import { View, StyleSheet, TouchableOpacity, TouchableWithoutFeedback, Keyboard } from "react-native"
import { Text, Button, Input } from "react-native-elements"
import { firebase } from "../config"
import { MaterialIcons, EvilIcons } from "@expo/vector-icons"
import DismissKeyboard from "../components/DismissKeyboard"

export default function SignInScreen({ navigation, loggedIn, setLoggedIn }) {
    const [email, setEmail] = useState("ijuliie@aim.com")
    const [password, setPassword] = useState("password")

    // useEffect(() => {
        const login = () => {
            firebase.auth().signInWithEmailAndPassword(email, password).then((res) => {
                const uid = res.user.uid
                const usersRef = firebase.firestore().collection("users")

                usersRef.doc(uid).get().then((firestoreDocument) => {
                    const user = firestoreDocument.data()
                    navigation.navigate("Dashboard", { user })
                    setLoggedIn(true)
                    setEmail("")
                    setPassword("")
                }).catch((err) => {
                    alert(err)
                })
            }).catch(() => {
                alert("Username or password is incorrect.")
            })
        }
    //     login()
    // }, [])

    return (
        <DismissKeyboard>
            <View style={styles.container}>
                <Text h4>Sign In</Text>
                <View style={styles.input}>
                    <MaterialIcons style={styles.icon} size={20} name="email" />
                    <Input 
                        placeholder="Email"
                        style={styles.input}
                        autoCorrect={false}
                        autoCapitalize="none"
                        value={email}
                        onChangeText={(email) => setEmail(email)}
                    />
                </View>
                <View style={styles.input}>
                    <EvilIcons style={styles.icon} name="lock" size={20} />
                    <Input 
                        placeholder="Password"
                        style={styles.input}
                        secureTextEntry
                        autoCorrect={false} 
                        autoCapitalize="none"
                        value={password}
                        onChangeText={(password) => {setPassword(password)}}
                    />
                </View>
                <Button type="outline" title="Sign In" onPress={login} />
                <TouchableOpacity onPress={() => navigation.navigate("Reset")}>
                    <Text style={styles.forgot}>
                        Forgot password?
                    </Text>
                </TouchableOpacity>
                <View style={styles.account}>
                    <TouchableOpacity onPress={() => navigation.navigate("SignUp")}>
                        <Text style={styles.signin}>
                            Don't have an account? Sign up!
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        </DismissKeyboard>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginBottom: 90
    },
    input: {
        flexDirection: "row",
        alignItems: "center",
        marginRight: 35
    },
    icon: {
        marginBottom: 25,
        paddingLeft: 30
    },
    forgot: {
        marginTop: 10,
        fontWeight: "bold"
    },
    signin: {
        fontWeight: "bold",
        marginTop: 10
    }
})