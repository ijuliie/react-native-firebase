import React, { useState, useContext } from "react"
import { View, StyleSheet, TouchableOpacity, TouchableWithoutFeedback, Keyboard } from "react-native";
import { Text, Button, Input } from "react-native-elements"
import { firebase } from "../config"
import DismissKeyboard from "../components/DismissKeyboard"
import { MaterialIcons, Ionicons, EvilIcons } from "@expo/vector-icons"

export default function SignUpScreen({ navigation, loggedIn, setLoggedIn }) {
    const [name, setName] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [checkPassword, setCheckPassword] = useState("")

    const isLoggedIn = () => {
        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                // console.log("logged in")
                // context.setLoggedIn(true)
            }
        })
      }

    const createAccount = () => {
        if (password !== checkPassword) {
            alert("Passwords do not match")
            return
        }

        // call firebase auth's createUserWithEmailAndPassword API
        // this creates a new account in the firebase console
        firebase.auth().createUserWithEmailAndPassword(email, password).then((res) => {
            const uid = res.user.uid
            const data = {
                id: uid,
                email,
                name
            }
            const users = firebase.firestore().collection("users")

            // if registration is successful, firebase makes a collection called "users" and stores it into firestore
            users.doc(uid)
                .set(data)
                .then(() => {
                    isLoggedIn()
                    setLoggedIn(true)
                    navigation.navigate("Dashboard", { user: data })
                    setName("")
                    setEmail("")
                    setPassword("")
                    setCheckPassword("")
                })
                .catch((err) => {
                    alert(err)
            })
            .catch((err) => {
                alert(err)
            })
        })
    }

    return (
        <DismissKeyboard>
            <View style={styles.container}>
                <Text style={styles.header} h4>Register</Text>
                <View style={styles.input}>
                    <Ionicons style={styles.icon} size={20} name="md-person" />
                    <Input 
                        placeholder="Name"
                        autoCorrect={false}
                        value={name}
                        onChangeText={(name) => setName(name)}
                    />
                </View>
                <View style={styles.input}>
                    <MaterialIcons style={styles.icon} size={20} name="email" />
                    <Input 
                        placeholder="Email"
                        autoCorrect={false}
                        autoCapitalize="none"
                        value={email}
                        onChangeText={(email) => setEmail(email)}
                    />
                </View>
                <View style={styles.input}>
                    <EvilIcons style={styles.icon} name="lock" size={20} />
                    <Input 
                        placeholder="Password"
                        secureTextEntry
                        autoCorrect={false} 
                        autoCapitalize="none"
                        value={password}
                        onChangeText={(password) => {setPassword(password)}}
                    />
                </View>
                <View style={styles.input}>
                    <EvilIcons style={styles.icon} name="lock" size={20} />
                    <Input 
                        placeholder="Confirm Password"
                        secureTextEntry
                        autoCorrect={false} 
                        autoCapitalize="none"
                        value={checkPassword}
                        onChangeText={(password) => {setCheckPassword(password)}}
                    />
                </View>
                <Button 
                    title="Sign Up" 
                    onPress={createAccount}
                    type="outline"
                />
                <View style={styles.account}>
                    <TouchableOpacity onPress={() => navigation.navigate("SignIn")}>
                        <Text style={styles.signin}>
                            Already have an account? Sign In
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        </DismissKeyboard>

    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginBottom: 150
    },
    input: {
        flexDirection: "row",
        alignItems: "center",
        marginRight: 35
    },  
    icon: {
        marginBottom: 25,
        paddingLeft: 30
    },
    header: {
        marginBottom: 10
    },
    account: {
        paddingTop: 20,
        justifyContent: "center",
        alignItems: "center",
    },
    signin: {
        fontSize: 15,
        fontWeight: "bold"
    },
    button: {
        marginTop: 10
    }
})