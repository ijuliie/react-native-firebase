import React, { useState } from "react"
import { View, StyleSheet, TouchableOpacity } from "react-native"
import { Text, Button } from "react-native-elements"
import { firebase } from "../config"

export default function HomeScreen({ navigation }) {
    const [loggedIn, setLoggedIn] = useState(false)
    // const getUser = firebase.auth().currentUser.email

    const isLoggedIn = () => {
        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                setLoggedIn(true)
            }
        })
    }

    const signOut = () => {
        firebase.auth().signOut().then(() => {
            alert("Successfully signed out")
            navigation.navigate("SignIn")
        })
    }

    return (
        <View style={styles.container}>
            {/* {
                loggedIn ? 
                <>
                    <Text h4>Welcome {getUser}!</Text>
                    <Button style={styles.button} type="outline" title="Log out" onPress={signOut} />
                </>
                : <Text h4>Welcome!</Text>
            } */}
                <Button 
                    onPress={() => navigation.navigate("SignUp")} 
                    title="Sign Up" 
                    type="outline" 
                />
                <Button 
                    onPress={() => navigation.navigate("SignIn")} 
                    title="Sign In" 
                    type="outline" 
                    style={styles.button} 
                />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    button: {
        marginTop: 10
    }
})