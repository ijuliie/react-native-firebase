import React, { useState, useEffect } from "react"
import { View, StyleSheet } from "react-native"
import { firebase } from "../config"
import Cloud from 'react-native-word-cloud';

export default function WordCloudScreen({ navigation }) {
    const [list, setList] = useState([])

    useEffect(() => {
        const getWords = async () => {
            const snapshot = await firebase.firestore().collection("words").get()
            const words = snapshot.docs.map((doc) => doc.data())
            setList(words)
        }
        getWords()
    }, [])

    // useEffect(() => {
    //     const getWords = async () => {
    //         const snapshot = await firebase.firestore().collection("users").doc("FU2q48W1Ppdaqhe2RKGmhF97kuH3").collection("01").get()
    //         const words = snapshot.forEach((doc) => doc.data())
    //         console.log(words)
    //         setList(words)
    //     }
    //     getWords()
    // }, [])

    return (
        <View style={styles.container}>
            <Cloud keywords={list} scale={330} />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    }
})