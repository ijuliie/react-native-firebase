import React, { useState, useEffect } from "react"
import { View, StyleSheet, Text, FlatList, Image, SafeAreaView, ScrollView } from "react-native"
import { firebase } from "../config"

export default function WordListScreen() {
    const [list, setList] = useState([])

    useEffect(() => {
        const getWords = async () => {
            const snapshot = await firebase.firestore().collection("words").get()
            const words = snapshot.docs.map((doc) => doc.data())
            setList(words)
        }
        getWords()
    }, [])

    // const words = list.map((word) => {
    //     console.log("puppies", word.puppy)
    // })

    return (
        <ScrollView>
            <SafeAreaView style={{ flex: 1, alignItems: "center" }}>
                <FlatList  style={{ flex: 1 }}
                    data={list}
                    renderItem={({item}) => {
                        return (
                            <>
                                <Text>{item.keyword}</Text>
                                <Image style={{ height: 150, width: 175 }} source={{ uri: `${item.puppy}`}} />
                            </>
                        )
                    }}
                />
            </SafeAreaView>
        </ScrollView>
    )
}

const styles = StyleSheet.create({})