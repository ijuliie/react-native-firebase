import React from "react"
import { View, Text, StyleSheet } from "react-native"

export default function LoggedInScreen() {
    return (
        <View>
            <Text>
                You are logged in
            </Text>
        </View>
    )
}