import React, { useState, useEffect } from "react"
import { View, StyleSheet, TouchableOpacity } from "react-native"
import { Text, Button } from "react-native-elements"
import { firebase } from "../config"
import MapView, { PROVIDER_GOOGLE, Polygon, Circle } from "react-native-maps"
import { Marker } from "react-native-maps";
// import Circle from "../components/Circle"

export default function DashboardScreen({ navigation, loggedIn, setLoggedIn }) {
    const [coordinates, setCoordinates] = useState([])
    const [markers, setMarkers] = useState([
        {   
            id: 1,
            latitude: 33.853669,
            longitude: -118.013232,
        },
        {
            id: 2,
            latitude: 33.855288,
            longitude: -118.013766
        }, 
        {
            id: 3,
            latitude: 33.854504,
            longitude: -118.015306
        }
    ])

    const [circles, setCircles] = useState([
        {
            id: 1,
            latitude: 33.853669,
            longitude: -118.013232,
            radius: 50,
            strokeWidth: 3
        },
        {
            id: 2,
            latitude: 33.855288,
            longitude: -118.013766,
            radius: 50,
            strokeWidth: 3
        },
        {
            id: 3,
            latitude: 33.854504,
            longitude: -118.015306,
            radius: 50,
            strokeWidth: 3
        },
    ])

    useEffect(() => {
        if(navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                position => {
                    const getLocation = position;
                    setCoordinates(getLocation.coords)
                })
        }
    }, [])


    const multipleMarkers = markers.map((marker) => {
        return (
            <Marker
                key={marker.id}
                coordinate={{
                    latitude: marker.latitude,
                    longitude: marker.longitude
                }}
            />
        )
    })


    const multipleCircles = circles.map((circle) => {
        return (
            <Circle 
                key={circle.id}
                center={{
                    latitude: circle.latitude,
                    longitude: circle.longitude
                }}
                radius={50}
                strokeWidth={5}
                strokeColor={'#1a66ff'}
                zIndex={1}
            />
        )
    })

    const signOut = () => {
        firebase.auth().signOut().then(() => {
            alert("Successfully signed out")
            setLoggedIn(false)
            navigation.navigate("Home")
        })
    }

    return (
        <View style={styles.container}>
            {
                !loggedIn ? <Text>You must be logged in</Text> :
                <>
                    {
                        coordinates ? 
                            <MapView 
                                provider={PROVIDER_GOOGLE}
                                style={{width: "100%", height: 500}}
                                region={{
                                    latitude: coordinates.latitude,
                                    longitude: coordinates.longitude,
                                    latitudeDelta: 0.0122,
                                    longitudeDelta: 0.0121
                                }}
                                showsUserLocation={true}
                            >
                            { multipleMarkers }
                            { multipleCircles }
                            {/* <Circle 
                                center={{
                                    latitude: 33.853669,
                                    longitude: -118.013232,
                                }}
                                radius={50}
                                strokeWidth={3}
                                strokeColor={'#1a66ff'}
                                fillColor={"rgba(255, 125, 178, 0.5)"}
                            /> */}
                            {/* <Polygon 
                                strokeWidth={1}
                                strokeColor="#000"
                                coordinates={[
                                    {latitude: 33.853669, longitude: -118.013232},
                                    {latitude: 33.855288, longitude: -118.013766},
                                    {latitude: 33.854504, longitude: -118.015306},
                                    {latitude: 33.853669, longitude: -118.013232}
                            ]} /> */}
                            </MapView> : ""
                    }
                    <Text h4>Welcome {firebase.auth().currentUser.email}!</Text>
                    <Button style={styles.button} type="outline" title="Log out" onPress={signOut} />
                </>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    button: {
        marginTop: 10
    }
})