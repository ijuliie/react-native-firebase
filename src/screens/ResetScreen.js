import React, { useState } from "react"
import { StyleSheet, View, TouchableWithoutFeedback, Keyboard } from "react-native"
import { Input, Button, Text } from "react-native-elements"
import { firebase } from "../config"
import { MaterialIcons } from "@expo/vector-icons"
import DismissKeyboard from "../components/DismissKeyboard"

export default function ResetScreen() {
    const [email, setEmail] = useState("")
    const [errorMessage, setErrorMessage] = useState("")

    const resetPassword = () => {
        firebase.auth().sendPasswordResetEmail(email).then(() => {
            // if successful
            alert("Please check your email for a reset link.")
        }).catch(() => {
            setErrorMessage("We can't seem to find that email.")
        })
    }

    return (
        <DismissKeyboard>
            <View style={styles.container}>
                <Text style={styles.header} h4>Forgot password?</Text>
                <View style={styles.inputContainer}>
                    <MaterialIcons style={styles.icon} size={20} name="email" />
                    <Input 
                        onChangeText={(email) => setEmail(email)}
                        placeholder="Email"
                        autoCapitalize="none"
                        autoCorrect={false} 
                    />
                </View>
                <View style={styles.errorMessageContainer}>
                    { errorMessage ? <MaterialIcons style={styles.errorIcon} size={15} name="error-outline" /> : null }
                    <Text style={styles.errorMessage}>{errorMessage}</Text>
                </View>
                <Button 
                    style={styles.button} 
                    title="Send Email" 
                    type="outline"
                    onPress={() => resetPassword()}
                />
            </View>
        </DismissKeyboard>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        marginBottom: 70,
    },
    errorMessageContainer: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"
    },
    errorIcon: {
        marginLeft: 8,
        color: "red",
    },
    errorMessage: {
        marginLeft: 10,
        color: "red",
    },
    header: {
        marginLeft: 10,
        marginBottom: 10
    },
    inputContainer: {
        flexDirection: "row",
        alignItems: "center",
        marginRight: 50
    },
    icon: {
        marginBottom: 25,
        paddingLeft: 5
    },
    button: {
        marginVertical: 20,
        alignSelf: "center"
    }
})