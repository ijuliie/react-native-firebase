import * as firebase from "firebase"
import "@firebase/auth"
import "@firebase/firestore"

const config = {
    apiKey: "AIzaSyB9oYkL8gvB-_-K0zTcuJHzOn2UPbx8I20",
    authDomain: "word-cloud-project.firebaseapp.com",
    databaseURL: "word-cloud-project.firebaseio.com",
    projectId: "word-cloud-project",
    storageBucket: "word-cloud-project.appspot.com",
    messagingSenderId: "766136566992",
    appId: "1:766136566992:ios:f50e8f10ba92350b524ad5"
}

if (!firebase.apps.length) {
    firebase.initializeApp(config);
}

export { firebase }