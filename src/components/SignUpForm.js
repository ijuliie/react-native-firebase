import React, { useState } from "react"
import { Text, TextInput, View, StyleSheet, Button } from "react-native";
import { firebase } from "../config"

export default function SignUpScreen({ navigation }) {
    const [name, setName] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [checkPassword, setCheckPassword] = useState("")

    const createAccount = () => {
        if (password !== checkPassword) {
            alert("Passwords do not match")
            return
        }

        // call firebase auth's createUserWithEmailAndPassword API
        // this creates a new account in the firebase console
        firebase.auth().createUserWithEmailAndPassword(email, password).then((res) => {
            const uid = res.user.uid
            const data = {
                id: uid,
                email,
                name
            }
            const users = firebase.firestore().collection("users")

            // if registration is successful, firebase makes a collection called "users" and stores it into firestore
            users.doc(uid)
                .set(data)
                .then(() => {
                    navigation.navigate("Home", { user: data })
                }).catch((err) => {
                    alert(err)
            })
            .catch((err) => {
                alert(err)
            })
        })
    }

    return (
        <View>
            <Text style={styles.label}>Name:</Text>
            <TextInput 
                style={styles.input}
                autoCorrect={false}
                value={name}
                onChangeText={(name) => setName(name)}
            />
            <Text style={styles.label}>Email:</Text>
            <TextInput 
                style={styles.input}
                autoCorrect={false}
                autoCapitalize="none"
                value={email}
                onChangeText={(email) => setEmail(email)}
            />
            <Text style={styles.label}>Password:</Text>
            <TextInput 
                style={styles.input}
                secureTextEntry
                autoCorrect={false} 
                autoCapitalize="none"
                value={password}
                onChangeText={(password) => {setPassword(password)}}
            />
             <Text style={styles.label}>Confirm Password:</Text>
            <TextInput 
                style={styles.input}
                secureTextEntry
                autoCorrect={false} 
                autoCapitalize="none"
                value={checkPassword}
                onChangeText={(password) => {setCheckPassword(password)}}
            />
            <Button title="Sign Up" onPress={createAccount} />
        </View>
    )
}

const styles = StyleSheet.create({
    label: {
        paddingLeft: 5
    },
    input: {
        borderColor: "black",
        borderWidth: 1,
        margin: 5
    }
})